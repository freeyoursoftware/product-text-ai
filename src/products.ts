import { Product } from './typings'

const products: Product[] = [
	{
		id: 1,
		name: 'Product 1',
		price: 100,
	},
	{
		id: 2,
		name: 'Product 2',
		price: 200,
	},
	{
		id: 3,
		name: 'Product 3',
		price: 300,
	}
]

export async function getProduct(id: number): Promise<Product> {
	const product = products.find(product => product.id === id);
	if(product) {
		return product;
	}
	throw new Error('Product not found');
}
import { Configuration, OpenAIApi } from 'openai'
import { OPENAI_API_KEY } from './config/openai.config'

const configuration = new Configuration({
	apiKey: OPENAI_API_KEY,
})

const openai = new OpenAIApi(configuration)

export async function getProductDescription(productName: string) {
	const result = await openai.createCompletion({
		model: 'text-davinci-003',
		max_tokens: 2000,
		prompt: `This is a test of GPT-3\n\nProduct: ${productName}`,
	})
	return result.data.choices[0].text
}
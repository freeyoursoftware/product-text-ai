import express from 'express'
import morgan from 'morgan'
import { getProductDescription } from './ai'
import { getProduct } from './products'

const app = express()

app.use(morgan('combined'))

app.get('/product/:id', async (req, res) => {
	try{
		const product = await getProduct(parseInt(req.params.id))
		const description = await getProductDescription(product.name)
		res.send({...product, description})
	} catch (err: any) {
		res.status(404).send(err.message)
	}
})

app.listen(3000, () => {
	console.log('li-x product AI is running on port 3000')
})
# li-x Product Text AI
   
1) Register at OpenAI and get an API key
2) Build the docker image using the docker-compose file
3) Set the environment variable OPENAI_API_KEY
4) Describe what routes the API provides and what they do
5) Use the API to generate a product description for a given product. Ideally you create a curl command. If you are unfamiliar with curl please describe what tool you use to use an API.
6) Change the product names to names of software products and adjust the description generation query to optimize the generated texts.
7) Add a cache feature so that the API will only generate each description once and instead return the previously generated one for the following requests.